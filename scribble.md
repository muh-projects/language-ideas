scribble
===

> Sensible, fast scripting language.

# Goals

- Succinct, intuitive, consistent language.
  - no dynamic `this` binding
  - no `__stuff(self, x,y)` for private methods
  - no `lambda x, y: x + y`
  - no `$` all over the place
  - no random workers that you need feed .js files to
  - no `requirements.txt` and no `package.json` with 10TB of node modules
- There is mostly just 1 right way to do something.
  - standard formatting
  - few redundant/alternative syntactical forms
- "Small", fast interpreter
  - Easily embeddable (for games) and nice API
- Optional compilation
  - static type checking
  - AOT generation of bytecode as supplement for faster execution

# Language

## Imports
```py
uses 
    mylib
    linalg::Vector, Matrix as Mat  # imort multiple symbols, alias
    gfx::                          # can be multiline for long lists
        Window as W
        Buffer
```

## Literals

```py

# line comment
#{ 
    multiline comment
}# 

true              # !false
1                 # integer number (32 bit)
1.0               # floating point number (32 bit)
10e3              # float, scientific
"wat"             # string
f"this is x: {x}" # template string

["a","b","c"] # vector (or array on stack if verifiably small and doesn't leave scope)
{1,2,3}       # set
(1,2,3)       # tuple

# map
{
    1 -> "value",
    3 -> "other value",
    x -> "wat" # uses value of "x" as key
}
# {(1, "value")} will also work, `->` just constructs tuples
```

### Other numeric types

```py
# native
f64(3.0) # 64 bit floating point
i64(3)   # 64 bit integer

# stdlib
real(1.0)     # arbitrary precision number
complex(1, 3) # complex number

# ranges
1:3   # 1,2,3
:3    # 0,1,2,3
3:    # 3,4,.. (containers will implement this as `3,4,..,$end-1,$end`)
-1:   # ..,1,0 (containers will implement this as `$end,$end-1,..,1,0`
1::2  # from 1 to the end of the container, in steps of 2
```

## Variables
```py
var n: Int = 10      # mutable
n = 20               # assignment
x: Int = 3           # immutable
y = 4                # inferred, collides with assignment; TODO
`a special name` = 5 # name it whatever
```

## Control structures

### Conditional

```py
if a == b then
    doStuff()
else if x then
    wat
else 
    println("¯\_( ツ )_/¯")
```

### Loops

```py
# loop is a plain monadic comprehension as in scala
for x <- 1:3
    println(x)

# TODO express this as monadic comprehension or function
while y
    y = # ...
```

### Monadic comprehensions

```text
for
    x <- monadX
    y <- monadY(x)
    r = 3
    y(r)
```

compiles to

```text
monadX.flatMap(monadY(_).flatMap(y =>
    r = 3
    y(r)
))
```

Similarly, set / list comprehensions are also monadic comprehensions:

```text
primesSquared = for
    x <- 1:100
    if x.isPrime()
    x*x
```

compiles to

```text
primesSquared = (1:100).filter(_.isPrime()).map(_0*_0)
```

## Functions

```py
# explicit types
myFun(x: Str): Int :=
    x.len()

# inferred
myFun(x) := x.len()

# pattern matching (WiP)
norm(c: Complex) := norm(c.toVec())
norm(Vec2(x,y)) := sqrt(x^2, y^2)
# ^^^^^ this is a shorthand for
# norm(v: Vec2(x: Float, y: Float)) := sqrt(x^2, y^2)

# generics with least upper bounds
# / trait implementation.. not yet 
# sure about implementation of OOP
add<T: Ring>(t1, t2) := t1 + t2
```

- Last expression is returned.
- Structures such as `if` and `for` can contain a `return` if inside a function definition for early returns.
- All functions are values applied to tuples, like in F#.
  - I am considering making functions just a big pattern match, like
    - matching on tuples `f (a,b) = #..`
    - matching on vectors `f [a,b] = #..`
    - matching on maps `f {a -> b, c -> d} = # ...`
    
    Example:

    ```py
    factorial[1] := 1
    factorial[2] := 2
    factorial[3] := 6
    factorial[x] := x * factorial[x-1]
    ```

### Anonymous Functions

```py
# explicit lambda
[1,2,3].map((x: int) => x.toString())

# inferred arg type (omit parens when 1 param)
[1,2,3].map(x => x.toString())

[1,2,3].map(x =>
    println(x)
    x*2
) # marks end of multiline lambda

# positional param access
[1,2,3].reduce(_0 + _0)

# blank param, _ = _0
[1,2,3].map(_.toString())
```

Basically, `_.toString()` compiles to `_0.toString()`, which compiles to `(x0) => x0.toString()`.

## Classes
```py
class Person
    .address: Str

    # can be invoked via `Person("nik", 69)`
    # automatically declares .name and .age members
    constructor(.name: Str, .age = 0) :=
        println(f"Name: {name}")
        address = ""
    
    .announce() :=
        println(f"Hello, I am {name}")
        a = if adult() then " not " else " "
        println(f"I am {age} years old, meaning I am{a}an adult")

    # private method
    priv .adult() := age > 18

    # short hand for private block
    # priv
    #    .adult() = age > 18
    #    .otherPrivMethod() = ...

# instantiate:

# generated constructor with all pub fields
Person(name="nik", age=69, address="wat")
Person("nik", 69) # explicit `.new` constructor

# traits can be implemented externally
trait Ordered<T>
    > (t: T): Boolean

impl Ordered<Person>
    > (that) := this.name > that.name

impl Hash<Person>
    .hash() := name.hash()

# type params can be constrained by traits
class Set<T: Ordered>
    priv .data: Tree<T>

Set().add(Person("nik",69)) # works, Person is ordered
Set().add(myRoom)           # doesn't work, my room is not ordered
```

### Coercion

```py
({ "name" -> "nik", "age" -> 69}: Person)
# is equal to
Person("nik", 69)
```

If the script is compiled, this only works if the map verifiably has 
fields that match the members of the type in question. Further, the
values associated with the keys must be of the same type as the respective
type member.

### Operators

Operators are regular functions, but without `.` prefix (which is the regular method accessor)

```py
class PolarCoords
    .radius: Float 
    .angle: Float

    ::angle(x,y): Float # static, PolarCoords::angle()

class Complex
    .re: Float
    .im: Float

    # most operators are regular functions
    +(c: Complex) := # ...
    -(c) := # ...

    # apply and index are special nuggets
    (i: Int) :=   # can be "invoked" i.e. Complex()(3)
    [r: Range] := # myComplex[1:3], whatever sense that would make lel
    [x:y] :=      # using pattern matching to match upper and lower bound of range
    [a, b] :=     # multiple params also possible, of course
```

### Pattern Matching for Objects

The following snippet extends the definition of `Complex` (!) from above.

```py
    # keyword for matching; allows extracting polar coords from complex repr.
    match: PolarCoords(Float, Float) :=
        PolarCoords(sqrt(re*re + im*im), PolarCoords::angle(re,im))

    # a match for the constructor is automatically generated
    #   match someComplex()
    #       Complex(im, re) => ...
    # will invoke the generated
    # match this(Float, Float) := (re, im)

    # alternative this-matching
    match: this(Float) :=
        match this
            PolarCoords(rad, _) => (rad)
    # for this case:
    #   match someComplex
    #       Complex(radius) => println(radius)
```

## Enums (WiP)

```py
enum ErrorCodes
    Aborted
    ConnectionReset
    Timeout
    Other(.message: Str)

enum Option<T>
    Some(t: T)
    None

# `uses Option::*` required to access directly
#  -> enum variants are basically static members
#  -> can be imported
#  -> Some and None will be imported by predef tho

impl Monad<Option, T>
    ::unit(t: T) := Some(t)
    .map(f) := flatMap(f.map(unit))   
    .flatMap(f) :=
        match this
            Some(t) => f(t)
            else => None
```


## Coroutines & Channels, Async / Await (WiP)

```py
runProdCons() :=
    ch = channel<Str>(buffer = 2)
    async produce(ch)                           # either specify async here (1/2)
    cr = consume(ch)                            # cr is a regular object of type Future<T> or whatever
                                                #   which is returned from `async` blocks/contexts

    if cr.thread() == currentThread() then      # this code here doesn't make any sense logically, but
        cr.cancel()                             # shows that `cr` is just a handle for the workload
                                                # and can be interacted with.

produce(ch) :=
    for i <- 1:3
        ch.send(i)
    ch.close()                                  # emits "EOF" signal

consume(ch) := async                             # or here (2/2)
    ch.foreach(println)                         # waits for channel "EOF" and suspends while
                                                # no values are available. returns on EOF

get(url) = async@cr                             # named block context
    s = Socket()
    s.connect(url)                              # lets pretend this is synchronous
    s.write("löl").end()                        # -,,-
    if !cr.cancelled() then                     # block context `cr` used to inquire about runtime state
        r = await s.read()                      # "await" yields until async op is done
        Ok(r)
    else
        Error(ErrorCodes.Aborted)
```

## Remarks

- All multiline syntactic forms such as `if`, `for`, `while`, functions, classes, etc. can have optional `end` token for readability.
- All forms of function definitions - functions, methods, constructors - can have default arguments.
- All forms of function invocations - functions, methods, constructors - can use labeled arguments.

# TODO

## Language facilities
- null safety & rust-like "? = return Err(xyz) if error occurred, else continue"
- block contexts (check async example)
- raising errors
  - panics and result instead of exceptions?
    -> maybe too low-level? you would have to deal with the result stuff

## Libraries

> Keep std super slim!! Also maybe call it `lead` since the "core" is called a pencil lead, would be a nice pun for a lang called "scribble" lül. I should really study instead of doing this.

> what will be part of predef?

- Str
- Maybe / Either
- certain math functions
- `->` for tuple construction
- `while` if it actually is implemented as a function eventually
- ...

### std

- strings
- containers
- i/o
  - files
  - sockets
    - udp, tcp, http, but only base protocols
  - stdin/stdout
- basic multithreading (channels, buffers, ...)
- basic math
- signals and slots? (nice for event propagation, multithreading, ...)

### External

Don't bloat std with these libs:

- proper HTTP interface for service creation
- advanced maths like statistics, linear algebra, etc.
- hashing & crypto
- multithreading (os threads, locks)
- db drivers
- windowing w/ basic graphics context
- basic graphics API for drawing primitives ontop of windowing
- plotting lib
- gui widgets & reactive / MVC framework
